// variable para la ventana de nuevo mensaje
let ventanamensaje;
// intervalo para recorrer la funcion cambiar estado cada 30 seg
var intervalID = window.setInterval(cambiarEstado, 30000);

//indicacion para la muestra de la ventana de nuevo mensaje
function nuevoMensaje() {
  ventanamensaje = window.open("send.html","ventana1","width=400,height=500,scrollbars=yes", "resizable");
  ventanamensaje.moveTo(500, 100);
}

//Metodo para agregar el mensaje al local storage
function AgregarMensaje(){
  var mreceptor = document.getElementById("receptor").value;
  var masunto = document.getElementById("asunto").value;
  var mmensaje = document.getElementById("mensaje").value;
  let mnewMensaje = {
    emisor : sessionStorage.user,
    receptor : mreceptor,
    asunto : masunto,
    mensaje : mmensaje,
    estado : "enviado",
    timer : "si"
  };
  localStorage.setItem(buscarID(), JSON.stringify( mnewMensaje ));

  let mnewMensaje2 = {
    emisor : sessionStorage.user,
    receptor : mreceptor,
    asunto : masunto,
    mensaje : mmensaje,
    estado : "entrada",
    timer : "si"
  };
  localStorage.setItem(buscarID(), JSON.stringify( mnewMensaje2 ));
  ventanamensaje = window.close();
}

// cambia el estado del mensaje con la cuenta del intervalo
function cambiarEstado(){
  for (x=0; x<=localStorage.length-1; x++){
    clave = localStorage.key(x);
    let mensaje= JSON.parse(localStorage.getItem( clave ));
    if(mensaje.timer == "si"){
      mensaje.timer = "no";
      localStorage.setItem(clave, JSON.stringify( mensaje ));
    }
  }
}

// Carga mensajes enviados
function cargarMensajesEnviados(){
  limpiarLista();
  for (x=0; x<=localStorage.length-1; x++){
    clave = localStorage.key(x);
    let user= JSON.parse(localStorage.getItem( clave ));
    if(user.emisor == sessionStorage.user){
      if(user.estado == "enviado" & user.timer == "no"){
        var tarea = "Para:  " + user.receptor,
        nuevaTarea = document.createElement("li"),
        enlace = document.createElement("a"),
        contenido = document.createTextNode(tarea);

        enlace.appendChild(contenido);
        enlace.setAttribute("href", "#");
        enlace.setAttribute("class","dato list-group-item list-group-item-info");
        enlace.setAttribute("value",clave);

        const button = document.createElement('button');
        button.type = 'button';
        button.setAttribute("class","btn btn-primary");
        button.innerText = 'Eliminar';
        button.setAttribute("onclick","mostrar('enviado')");

        nuevaTarea.appendChild(enlace);
        nuevaTarea.appendChild(button);
        lista.appendChild(nuevaTarea);

        for (var i = 0; i <= lista.children.length -1; i++) {
          lista.children[i].addEventListener("click", function(){
            let mensaje= JSON.parse(localStorage.getItem( this.childNodes[0].getAttribute("value") ));
            var mAsunto = document.getElementById("m-asunto").value = mensaje.asunto;
            var mMensaje = document.getElementById("m-mensaje").value = mensaje.mensaje;
          });
        }

      }

    }

  }


}

//Cargar mensaje de espera
function cargarMensajeEspera(){
  limpiarLista();
  for (x=0; x<=localStorage.length-1; x++){
    clave4 = localStorage.key(x);
    let mensajeEspera= JSON.parse(localStorage.getItem( clave4 ));
    if(mensajeEspera.estado == "enviado" & mensajeEspera.timer == "si"){

      var tarea = "Para:  " + mensajeEspera.receptor,
      nuevaTarea = document.createElement("li"),
      enlace = document.createElement("a"),
      contenido = document.createTextNode(tarea);

      enlace.appendChild(contenido);
      enlace.setAttribute("href", "#");
      enlace.setAttribute("class","dato list-group-item list-group-item-info");
      enlace.setAttribute("value",clave4);

      const button = document.createElement('button');
      button.type = 'button';
      button.setAttribute("class","btn btn-primary");
      button.innerText = 'Eliminar';
      button.setAttribute("onclick","mostrar('espera')");

      nuevaTarea.appendChild(enlace);
      nuevaTarea.appendChild(button);
      lista.appendChild(nuevaTarea);

      for (var i = 0; i <= lista.children.length -1; i++) {
        lista.children[i].addEventListener("click", function(){
          let mensaje= JSON.parse(localStorage.getItem( this.childNodes[0].getAttribute("value") ));
          var mAsunto = document.getElementById("m-asunto").value = mensaje.asunto;
          var mMensaje = document.getElementById("m-mensaje").value = mensaje.mensaje;
        });
      }

    }
  }
}

// Carga mensaje de entrada
function cargarBandejaEntrada(){
    limpiarLista();
  for (x=0; x<=localStorage.length-1; x++){
    clave2 = localStorage.key(x);
    let user2= JSON.parse(localStorage.getItem( clave2 ));
    if(user2.receptor == sessionStorage.user){
      if(user2.estado == "entrada" & user2.timer == "no"){
        var tarea = "De:  " + user2.emisor,
        nuevaTarea = document.createElement("li"),
        enlace = document.createElement("a"),
        contenido = document.createTextNode(tarea);

        enlace.appendChild(contenido);
        enlace.setAttribute("href", "#");
        enlace.setAttribute("class","dato")
        enlace.setAttribute("value",clave2);

        const button = document.createElement('button');
        button.type = 'button';
        button.setAttribute("class","btn btn-primary");
        button.innerText = 'Eliminar';
        button.setAttribute("onclick","mostrar('entrada')");


        nuevaTarea.appendChild(enlace);
        nuevaTarea.appendChild(button);
        lista.appendChild(nuevaTarea);

        for (var i = 0; i <= lista.children.length -1; i++) {
          lista.children[i].addEventListener("click", function(){
            let mensaje= JSON.parse(localStorage.getItem( this.childNodes[0].getAttribute("value") ));
            var mAsunto = document.getElementById("m-asunto").value = mensaje.asunto;
            var mMensaje = document.getElementById("m-mensaje").value = mensaje.mensaje;
          });
        }

    }

  }

}
}


//Eliminar los mensajes selecionados
function mostrar(tipo){
  if(tipo == "enviado"){
    for (var i = 0; i <= lista.children.length -1; i++) {
      lista.children[i].addEventListener("click", function(){
        localStorage.removeItem(this.childNodes[0].getAttribute("value"));
        cargarMensajesEnviados();
      });
    }
  }else if(tipo == "espera"){
    for (var i = 0; i <= lista.children.length -1; i++) {
      lista.children[i].addEventListener("click", function(){
        let numID = this.childNodes[0].getAttribute("value");
        localStorage.removeItem(numID);
        localStorage.removeItem(parseInt(numID) + 1);
        cargarMensajeEspera();
      });

    }
  }else {
    for (var i = 0; i <= lista.children.length -1; i++) {
      lista.children[i].addEventListener("click", function(){
        localStorage.removeItem(this.childNodes[0].getAttribute("value"));
        cargarBandejaEntrada();
      });
  }
}
}


// busca la utlima id registrada para continuar registrando mensajes
function buscarID(){
  id = 0;
  for (x=0; x<=localStorage.length - 1; x++){
    clave3 = localStorage.key(x);
    let mensaje3 = JSON.parse(localStorage.getItem( clave3 ));
    if(mensaje3.estado == "enviado" || mensaje3.estado == "entrada"){
      if(parseInt(clave3) > id){
        id = clave3;
      }

    }

  }
  return id = parseInt(id) +1;
}

// limpia la lista
function limpiarLista(){
  try {
    console.log(lista.children.length);
    for (var i = 0; i <= lista.children.length + i; i++) {
      lista.removeChild(lista.childNodes[0]);
      console.log("borro");
      }
  } catch (e) {

  } finally {

  }
  }
