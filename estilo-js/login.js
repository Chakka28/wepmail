
 // botones para el evento
let bottonAdd = document.getElementById("boton-guardar");
let bottonLogin = document.getElementById("boton-login");

// carga el usuario y la guarda en session storage
function cargar(){
  console.log("si cargo");
  var uUser = document.getElementById("form-username-login").value;
  var uContrasena = document.getElementById("form-password-login").value;
  let user= JSON.parse(localStorage.getItem( uUser ));
  sessionStorage.user = user.user;
}

// registra el usuario
bottonAdd.addEventListener("click", function(){
  console.log("listo");
  var uName = document.getElementById("form-name").value;
  var uUser = document.getElementById("form-username").value;
  var uContrasena = document.getElementById("form-password").value;
  var reeContrasena = document.getElementById("form-password1").value;
  if(uContrasena == reeContrasena){
    let newUser = {
      name : uName,
      user : uUser,
      pass : uContrasena
    };
    localStorage.setItem(uUser, JSON.stringify( newUser ));
    document.getElementById("form-name").value = "";
    document.getElementById("form-username").value = "";
    document.getElementById("form-password").value = "";
    document.getElementById("form-password1").value = "";
    alert("Usuario Agregado")
  }else {
    alert("Contraseñas no conciden")
  }
  });


// valida el usuario ingresado a la plataforma
bottonLogin.addEventListener("click", function(){
  console.log("aqui");

  var uUser = document.getElementById("form-username-login").value;
  var uContrasena = document.getElementById("form-password-login").value;

  if(localStorage.getItem( uUser )){
    let user = JSON.parse(localStorage.getItem( uUser ));
    if(user.pass == uContrasena){
      document.location.href="Entry.html";
    }else {
      alert("Datos no conciden");
    }

  }else {
    alert("Usuario no existe");
  }
});
